package week2.assignment2;

public class BowAndArrowBehavior implements WeaponBehavior {
    @Override
    public void useWeapon() {
        System.out.println("I am using Bow and Arrow!");
    }
}
