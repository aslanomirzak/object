package week2.assignment2;

public interface WeaponBehavior {
    void useWeapon();
}
