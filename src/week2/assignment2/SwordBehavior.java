package week2.assignment2;

public class SwordBehavior implements WeaponBehavior {
    @Override
    public void useWeapon() {
        System.out.println("I am using Sword!");
    }
}
