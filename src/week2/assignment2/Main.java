package week2.assignment2;

public class Main {
    public static void main(String[] args) {
        King king = new King();
        king.setWeapon(new AxeBehavior());
        king.fight();
        Knight knight = new Knight();
        knight.fight();
    }
}
