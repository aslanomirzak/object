package week2.assignment2;

public abstract class Character {
    private WeaponBehavior weapon;

    public void fight(){
        weapon.useWeapon();
    }

    public void setWeapon(WeaponBehavior weapon) {
        this.weapon = weapon;
    }
}
