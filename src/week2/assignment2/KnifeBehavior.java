package week2.assignment2;

public class KnifeBehavior implements WeaponBehavior {
    @Override
    public void useWeapon() {
        System.out.println("I am using Knife!");
    }
}
