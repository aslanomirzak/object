package week4.assignment4;

public class Main {
    public static void main(String[] args) {
        Gun gun1 = new Gun("AK-74");
        gun1.connectMagazine();
        gun1.switchOffPredohranitel();

        try {
            gun1.shoot();
        } catch (GunIsNotReadyException e){
            e.printStackTrace();
        }
    }
}
