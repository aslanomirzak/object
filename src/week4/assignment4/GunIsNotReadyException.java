package week4.assignment4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class GunIsNotReadyException extends Exception {
    public GunIsNotReadyException(String message){
        super(message);
        createNewFile(message);
    }
    public void createNewFile(String text) {
        try {
            FileOutputStream myFile = new FileOutputStream("Exception.txt");
            PrintWriter out = new PrintWriter(myFile);
            out.print(text);
            out.close();
        } catch (IOException e) {
            System.out.println("Don't write date to File");
        }
    }
}
