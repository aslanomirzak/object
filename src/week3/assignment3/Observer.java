package week3.assignment3;

public interface Observer {
    void update(double temp, double humidity, double pressure);
}
