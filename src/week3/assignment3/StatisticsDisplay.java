package week3.assignment3;

public class StatisticsDisplay implements Observer, DisplayElement {
    private double minTemp = 999;
    private double maxTemp = -273.15;
    private double sumTemp = 0;
    private double minHumid = 100;
    private double maxHumid = 0;
    private double sumHumid = 0;
    private double minPress = 2;
    private double maxPress = 0;
    private double sumPress = 0;
    private int count = 0;
    private Subject weatherData;

    public StatisticsDisplay(Subject weatherData){
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Weather Statistics:");
        System.out.println("Min Temperature: " + minTemp + "°C");
        System.out.println("Max Temperature: " + maxTemp + "°C");
        System.out.println("Avg Temperature: " + sumTemp/count + "°C");
        System.out.println("Min Humid: " + minHumid + "%");
        System.out.println("Max Humid: " + maxHumid + "%");
        System.out.println("Avg Humid: " + sumHumid/count + "%");
        System.out.println("Min Pressure: " + minPress + "atm");
        System.out.println("Max Pressure: " + maxPress + "atm");
        System.out.println("Avg Pressure: " + sumPress/count + "atm");
        System.out.println();
    }

    @Override
    public void update(double temp, double humidity, double pressure) {
        if(temp < minTemp){
            minTemp = temp;
        }
        if(temp > maxTemp){
            maxTemp = temp;
        }
        sumTemp = sumTemp + temp;

        if(humidity < minHumid){
            minHumid = humidity;
        }
        if(humidity > maxHumid){
            maxHumid = humidity;
        }
        sumHumid = sumHumid + humidity;

        if(pressure < minPress){
            minPress = pressure;
        }
        if(pressure > maxPress){
            maxPress = pressure;
        }
        sumPress = sumPress + pressure;

        count++;
        display();
    }

    public double getAvgTemp(){
        return sumTemp/count;
    }

    public double getAvgHumid(){
        return sumHumid/count;
    }

    public double getAvgPress(){
        return sumPress/count;
    }
}
