package week3.assignment3;

public class CurrentConditionsDisplay implements  Observer, DisplayElement{
    private double temperature;
    private double humidity;
    private double pressure;
    private Subject weatherData;

    public CurrentConditionsDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void update(double temperature, double humidity, double pressure) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        display()   ;
    }

    @Override
    public void display() {
        System.out.println("Current conditions:");
        System.out.println("Temperature: " + temperature + "°C");
        System.out.println("Humidity: " + humidity + "%");
        System.out.println("Pressure: " + pressure + "atm");
        System.out.println();
    }
}
