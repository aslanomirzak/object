package week3.assignment3;

public class ForecastDisplay implements Observer, DisplayElement {
    private double lastTemp = -273.15;
    private double lastHumid = 0;
    private double lastPress = 0;
    private int isMoreTemp;
    private int isMoreHumid;
    private int isMorePress;
    private Subject weatherData;

    public ForecastDisplay(Subject weatherData){
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Forecast:");
        if(isMoreTemp == 1){
            System.out.println("Temperature is increasing");
        } else if(isMoreTemp == -1){
            System.out.println("Temperature is decreasing");
        } else{
            System.out.println("Temperature is not changing");
        }

        if(isMoreHumid == 1){
            System.out.println("Humidity is increasing");
        } else if(isMoreHumid == -1){
            System.out.println("Humidity is decreasing");
        } else{
            System.out.println("Humidity is not changing");
        }

        if(isMorePress == 1){
            System.out.println("Pressure is increasing");
        } else if(isMorePress == -1){
            System.out.println("Pressure is decreasing");
        } else{
            System.out.println("Pressure is not changing");
        }
        System.out.println("______________________________");
    }

    @Override
    public void update(double temp, double humidity, double pressure) {
        if(temp > lastTemp){
            isMoreTemp = 1;
        }
        else if(temp < lastTemp){
            isMoreTemp = -1;
        }
        else{
            isMoreTemp = 0;
        }
        lastTemp = temp;

        if(humidity > lastHumid){
            isMoreHumid = 1;
        }
        else if(humidity < lastHumid){
            isMoreHumid = -1;
        }
        else {
            isMoreHumid = 0;
        }
        lastHumid = humidity;

        if(pressure > lastPress){
            isMorePress = 1;
        }
        else if(pressure < lastPress){
            isMorePress = -1;
        }
        else{
            isMorePress = 0;
        }
        lastPress = pressure;
        display();
    }
}
