package week1.hw;

public class Person {
    private String name;
    private int birthYear;

    public Person() {
    }

    public Person(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    public int age(){
        return 2020-getBirthYear();
    }

    public void input(String name, int birthYear){
        setName(name);
        setBirthYear(birthYear);
    }

    public void output(){
        System.out.println("Name: " + name + "; Birth Year: " + birthYear);
    }

    public void changeName(String newName){
        this.name = newName;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

}
