package week1.hw;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Aslan", 2000);
        Person person2 = new Person();
        person2.setName("Aidyn");
        person2.setBirthYear(2001);
        Person person3 = new Person();
        person3.input("Yerassyl", 1999);
        Person person4 = new Person("Yerkebulan", 2002);
        Person person5 = new Person("Artur", 2001);
    }
}
