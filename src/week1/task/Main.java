package week1.task;

public class Main {
    public static void main(String[] args) {
        Employee employee1 = new Employee();
        Employee employee2 = new Employee();
        Employee employee3 = new Employee();

        employee1.setName("Aslan");
        employee1.setRate(24.5);
        employee1.setHours(70);
        employee2.setName("Aidyn");
        employee2.setRate(30.1);
        employee2.setHours(65);
        employee3.setName("Yerassyl");
        employee3.setRate(28.7);
        employee3.setHours(67);

        System.out.println(Employee.totalHours);
    }
}
