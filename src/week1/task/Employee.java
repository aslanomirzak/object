package week1.task;

public class Employee {
    private String name;
    private double rate;
    private int hours;

    static int totalSum;
    static int totalHours = 0;

    public Employee() {
    }

    public Employee(String name, double rate) {
        this.name = name;
        this.rate = rate;
    }

    public Employee(String name, double rate, int hours) {
        this.name = name;
        this.rate = rate;
        this.hours = hours;
        totalHours+=hours;
    }

    public double salary(){
        return rate*hours;
    }

    @Override
    public String toString() {
        return "Name: " + name +
                ", rate = " + rate +
                ", hours = " + hours +
                '}';
    }

    public void changeRate(double newRate){
        this.rate = newRate;
    }

    public double bonuses(){
        return salary()*0.1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        totalHours=totalHours+hours-this.hours;
        this.hours = hours;
    }

    public static int getTotalSum() {
        return totalSum;
    }

    public static void setTotalSum(int totalSum) {
        Employee.totalSum = totalSum;
    }

}
