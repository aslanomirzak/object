package Factory;

public class PistolMaker implements GunMaker {
    @Override
    public Gun makeGun() {
        return new Pistol();
    }
}
