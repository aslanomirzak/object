package Factory;

public class Pistol implements Gun {
    @Override
    public void shoot() {
        System.out.println("I shoot using pistol");
    }
}
