package Factory;

public class Rifle implements Gun {
    @Override
    public void shoot() {
        System.out.println("I shoot using rifle");
    }
}
