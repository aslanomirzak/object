package Factory;

public class RifleMaker implements GunMaker{
    @Override
    public Gun makeGun() {
        return new Rifle();
    }
}
