package Factory;

public class Shotgun implements Gun{
    @Override
    public void shoot() {
        System.out.println("I shoot using shotgun");
    }
}
