package Factory;

public interface GunMaker {
    Gun makeGun();
}
