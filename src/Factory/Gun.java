package Factory;

public interface Gun {
    void shoot();
}
