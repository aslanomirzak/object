package Factory;

public class Main {
    public static void main(String[] args) {
        GunMaker gunMaker = getMakerByGun("Shotgun");
        Gun gun = gunMaker.makeGun();
        gun.shoot();
    }

    public static GunMaker getMakerByGun(String gun){
        if(gun.equals("Pistol")){
            return new PistolMaker();
        } else if (gun.equals("Shotgun")){
            return new ShotgunMaker();
        } else if(gun.equals("Rifle")){
            return new RifleMaker();
        }

        throw new RuntimeException(gun + " maker does not exist");
    }
}
