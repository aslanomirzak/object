package Factory;

public class ShotgunMaker implements GunMaker{
    @Override
    public Gun makeGun() {
        return new Shotgun();
    }
}
